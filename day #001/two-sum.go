/* 
task:
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.
*/

func twoSum(nums []int, target int) []int {
    for i:=0; i < len(nums); i++ {
        for j := i + 1; j < len(nums); j++ {
            if nums[i]+nums[j] == target{
                return []int{i,j}
            }
        }
        
    }
    return []int{}
}

/*
result:
Success!
Runtime: 71 ms, faster than 8.35% of Go online submissions for Two Sum.
Memory Usage: 3.6 MB, less than 95.09% of Go online submissions for Two Sum.
*/